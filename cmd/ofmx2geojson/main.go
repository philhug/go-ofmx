package main

import (
	"bytes"
	"encoding/xml"
	"fmt"
	"golang.org/x/net/html/charset"
	"io/ioutil"

	"github.com/paulmach/go.geojson"
	ofmx "gitlab.com/openflightmaps/go-ofmx/pkg/ofmx/types"
)

func AddToFmap(fmap ofmx.FeatureMap, features []ofmx.Feature) {
	for _, f := range features {
		_, ok := fmap[f.Uid().String()]
		if ok {
			fmt.Printf("Duplicate feature: %s\n", f.Uid().String())
		}
		fmt.Println(f.Uid().String())
		fmap[f.Uid().String()] = f
	}
}

func main() {
	var snap ofmx.OfmxSnapshot

	b, err := ioutil.ReadFile("./test/testdata/ofmx_lo.xml")
	if err != nil {
		panic(err)
	}

	decoder := xml.NewDecoder(bytes.NewReader(b))
	decoder.Strict = true
	decoder.CharsetReader = charset.NewReaderLabel

	err = decoder.Decode(&snap)
	if err != nil {
		panic("Cannot parse input file:" + err.Error())
	}

	fmap := make(ofmx.FeatureMap, 0)
	AddToFmap(fmap, snap.Features)
	fmt.Println(len(fmap))

	gjDpn := geojson.NewFeatureCollection()
	gjAhp := geojson.NewFeatureCollection()
	gjAbd := geojson.NewFeatureCollection()
	for _, f := range snap.Features {
		gf, ok := f.(ofmx.GeoFeature)
		if !ok {
			fmt.Printf("Not a GeoFeature, skipping: %s\n", f.Uid().String())
			continue
		}

		j, err := gf.GeoJson(fmap)
		if err != nil {
			fmt.Printf("Error, skipping: %s, %s\n", f.Uid().String(), err)
			continue
		}
		ofmx.FillProperties(gf, j)

		switch t := f.(type) {
		case *ofmx.Dpn:
			gjDpn.AddFeature(j)
		case *ofmx.Ahp:
			gjAhp.AddFeature(j)
		case *ofmx.Abd:
			if t.AbdUid.AseUid.CodeId == "LESMO AREA" {
				gjAbd.AddFeature(j)
			}
		default:
			continue
		}
	}
	js, _ := gjDpn.MarshalJSON()
	ioutil.WriteFile("navigation.geojson", js, 0644)
	js, _ = gjAhp.MarshalJSON()
	ioutil.WriteFile("airports.geojson", js, 0644)
	js, _ = gjAbd.MarshalJSON()
	ioutil.WriteFile("airspace.geojson", js, 0644)
}
