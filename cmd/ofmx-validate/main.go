package main

import (
	"bytes"
	"encoding/xml"
	"fmt"
	"io/ioutil"

	ofmx "gitlab.com/openflightmaps/go-ofmx/pkg/ofmx/types"
	"golang.org/x/net/html/charset"
	"gopkg.in/go-playground/validator.v9"
)

type validationError struct {
	Id    string
	Field string
	Error string
}

func AddToFmap(fmap ofmx.FeatureMap, features []ofmx.Feature) {
	for _, f := range features {
		_, ok := fmap[f.Uid().Hash()]
		if ok {
			fmt.Printf("Duplicate feature: %s\n", f.Uid().String())
		}
		fmap[f.Uid().Hash()] = f
	}
}

func main() {
	var snap ofmx.OfmxSnapshot
	var errorList []validationError
	validate := validator.New()
	fmap := make(ofmx.FeatureMap, 0)

	b, err := ioutil.ReadFile("./test/testdata/ofmx_lo.xml")
	if err != nil {
		panic(err)
	}

	decoder := xml.NewDecoder(bytes.NewReader(b))
	decoder.Strict = true
	decoder.CharsetReader = charset.NewReaderLabel

	err = decoder.Decode(&snap)
	if err != nil {
		panic("Cannot parse input file:" + err.Error())
	}
	AddToFmap(fmap, snap.Features)
	ofmx.UpdateReferences(fmap)

	for _, f := range snap.Features {

		uid := f.Uid()

		var rUid *ofmx.RegionalUid
		switch v := uid.(type) {
		case *ofmx.AdgUid:
			rUid = &v.AseUid.RegionalUid
		case *ofmx.MscUid:
			rUid = &v.AhpUid.RegionalUid
		case *ofmx.ApnUid:
			rUid = &v.AhpUid.RegionalUid
		case *ofmx.RwyUid:
			rUid = &v.AhpUid.RegionalUid
		case *ofmx.AseUid:
			rUid = &v.RegionalUid
		case *ofmx.AbdUid:
			rUid = &v.AseUid.RegionalUid
		case *ofmx.UniUid:
			rUid = &v.RegionalUid
		case *ofmx.FqyUid:
			rUid = &v.SerUid.UniUid.RegionalUid
		case *ofmx.AhuUid:
			rUid = &v.AhpUid.RegionalUid
		case *ofmx.SerUid:
			rUid = &v.UniUid.RegionalUid
		case *ofmx.SaeUid:
			rUid = &v.AseUid.RegionalUid
		case *ofmx.RddUid:
			rUid = &v.RdnUid.RwyUid.AhpUid.RegionalUid
		case *ofmx.RdnUid:
			rUid = &v.RwyUid.AhpUid.RegionalUid
		case *ofmx.TwyUid:
			rUid = &v.AhpUid.RegionalUid
		case *ofmx.TlyUid:
			rUid = &v.TwyUid.AhpUid.RegionalUid
		case *ofmx.GbrUid:
			continue
			//TODO
		}
		if rUid != nil {
			if rUid.Region == "" {
				rUid.Region = "LOVV"
			}
		}

		mid := f.Uid().OriginalMid()
		if uid.Hash() != mid {
			fmt.Printf("%s: %s/%s\n", uid.String(), uid.Hash(), mid)
		}

		err := validate.Struct(f)
		if err != nil {
			// this check is only needed when your code could produce
			// an invalid value for validation such as interface with nil
			// value most including myself do not usually have code like this.
			if _, ok := err.(*validator.InvalidValidationError); ok {
				fmt.Println(err)
				continue
			}

			//errs := err.(validator.ValidationErrors)
			//fmt.Println(errs)

			for _, err := range err.(validator.ValidationErrors) {
				errorList = append(errorList, validationError{Id: uid.String(), Field: err.StructNamespace(), Error: err.Param()})
			}

			//log.Printf("validation failed: %v", err)
		}
	}

	for _, e := range errorList {
		fmt.Printf("%s: %s %s\n", e.Id, e.Field, e.Error)
	}

}
