package main

import (
	"bytes"
	"encoding/xml"
	"fmt"
	flatten "github.com/doublerebel/bellows"
	"golang.org/x/net/html/charset"
	"io/ioutil"

	"github.com/paulmach/go.geojson"
	ofmx "gitlab.com/openflightmaps/go-ofmx/pkg/ofmx/types"
)

func AddToFmap(fmap ofmx.FeatureMap, features []ofmx.Feature) {
	for _, f := range features {
		_, ok := fmap[f.Uid().String()]
		if ok {
			fmt.Printf("Duplicate feature: %s\n", f.Uid().String())
		}
		//fmt.Println(f.Uid().String())
		fmap[f.Uid().String()] = f
	}
}

type Airport struct {
	ahp  ofmx.Ahp
	fuel []ofmx.Ful
}

func main() {
	var snap ofmx.OfmxSnapshot
	fmap := make(ofmx.FeatureMap, 0)
	fnames := []string{
		"test/testdata/ofmx/ofmx/ed/latest/isolated/ofmx_ed.xml",
		"test/testdata/ofmx/ofmx/fa/latest/isolated/ofmx_fa.xml",
		"test/testdata/ofmx/ofmx/lhcc/latest/isolated/ofmx_lh.xml",
		"test/testdata/ofmx/ofmx/ekdk/latest/isolated/ofmx_ek.xml",
		"test/testdata/ofmx/ofmx/lovv/latest/isolated/ofmx_lo.xml",
		"test/testdata/ofmx/ofmx/lzbb/latest/isolated/ofmx_lz.xml",
		"test/testdata/ofmx/ofmx/lbsr/latest/isolated/ofmx_lb.xml",
		"test/testdata/ofmx/ofmx/efin/latest/isolated/ofmx_ef.xml",
		"test/testdata/ofmx/ofmx/lkaa/latest/isolated/ofmx_lk.xml",
		"test/testdata/ofmx/ofmx/lsas/latest/isolated/ofmx_ls.xml",
		"test/testdata/ofmx/ofmx/ehaa/latest/isolated/ofmx_eh.xml",
		"test/testdata/ofmx/ofmx/epww/latest/isolated/ofmx_ep.xml",
		"test/testdata/ofmx/ofmx/ebbu/latest/isolated/ofmx_eb.xml",
		"test/testdata/ofmx/ofmx/lrbb/latest/isolated/ofmx_lr.xml",
		"test/testdata/ofmx/ofmx/li/latest/isolated/ofmx_li.xml",
		"test/testdata/ofmx/ofmx/ldzo/latest/isolated/ofmx_ld.xml",
		"test/testdata/ofmx/ofmx/ljla/latest/isolated/ofmx_lj.xml",
		"test/testdata/ofmx/ofmx/fywh/latest/isolated/ofmx_fy.xml",
		"test/testdata/ofmx/ofmx/esaa/latest/isolated/ofmx_es.xml",
	}

	for _, fname := range fnames {

		b, err := ioutil.ReadFile(fname)
		if err != nil {
			panic(err)
		}

		decoder := xml.NewDecoder(bytes.NewReader(b))
		decoder.Strict = true
		decoder.CharsetReader = charset.NewReaderLabel

		err = decoder.Decode(&snap)
		if err != nil {
			panic("Cannot parse input file:" + err.Error())
		}

		AddToFmap(fmap, snap.Features)
	}
	ofmx.UpdateReferences(fmap)
	//fmt.Println(len(fmap))

	airports := make(map[string]Airport)

	for _, f := range snap.Features {
		switch t := f.(type) {
		case *ofmx.Ahp:
			ap, ok := airports[t.AhpUid.String()]
			if !ok {
				ap = Airport{}
			}
			ap.ahp = *t
			airports[t.AhpUid.String()] = ap
		case *ofmx.Ful:
			ap, ok := airports[t.FulUid.AhpUid.String()]
			if !ok {
				ap = Airport{}
			}
			ap.fuel = append(ap.fuel, *t)
			airports[t.FulUid.AhpUid.String()] = ap
		default:
			continue
		}
	}

	gjAhp := geojson.NewFeatureCollection()
	for _, ap := range airports {
		ahp := ap.ahp
		var gf ofmx.GeoFeature
		gf = &ap.ahp

		j, err := gf.GeoJson(fmap)
		if err != nil {
			fmt.Printf("Error, skipping: %s, %s\n", ahp.Uid().String(), err)
			continue
		}
		ofmx.FillProperties(gf, j)

		for _, f := range ap.fuel {
			flatten.FlattenPrefixedToResult(f, f.FulUid.CodeCat+"_", j.Properties)
		}
		gjAhp.AddFeature(j)
	}

	js, _ := gjAhp.MarshalJSON()
	ioutil.WriteFile("fuelmap.geojson", js, 0644)
}
