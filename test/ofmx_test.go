package test

import (
	"encoding/xml"
	"github.com/stretchr/testify/assert"
	ofmx "gitlab.com/openflightmaps/go-ofmx/pkg/ofmx/types"
	"golang.org/x/net/html/charset"
	"os"
	"testing"
)

func TestExamples(t *testing.T) {
	testcases := []string{
		"airport.ofmx",
		"airspace.ofmx",
		"label_marker.ofmx",
		"landing_aid.ofmx",
		"navigational_aid.ofmx",
		"obstacle.ofmx",
		"organisation.ofmx",
		"timetable.ofmx",
	}
	ofmx.MidModeCompliant = true
	for _, tf := range testcases {
		f, err := os.Open("testdata/examples/" + tf)
		if err != nil {
			t.Error(err)
		}
		var snap ofmx.OfmxSnapshot
		decoder := xml.NewDecoder(f)
		decoder.Strict = true
		decoder.CharsetReader = charset.NewReaderLabel

		err = decoder.Decode(&snap)
		if err != nil {
			t.Error("Cannot parse input file:" + err.Error())
		}
		for _, f := range snap.Features {
			uid := f.Uid()
			assert.Equal(t, uid.OriginalMid(), uid.Hash(), "MID mismatch:"+uid.String())
		}
	}
}
