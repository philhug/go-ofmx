package types

import (
	"errors"
	"log"
	"reflect"
)

func NewFeature(name string) (Feature, error) {
	// Org/Frequency
	switch name {
	case "Org":
		return &Org{}, nil
	case "Uni":
		return &Uni{}, nil
	case "Ser":
		return &Ser{}, nil
	case "Fqy":
		return &Fqy{}, nil

	// Airport
	case "Ahp":
		return &Ahp{}, nil
	case "Ful":
		return &Ful{}, nil
	case "Rwy":
		return &Rwy{}, nil
	case "Rdn":
		return &Rdn{}, nil
	case "Rdd":
		return &Rdd{}, nil
	case "Rls":
		return &Rls{}, nil
	case "Ahu":
		return &Ahu{}, nil
	case "Ahs":
		return &Ahs{}, nil
	case "Aga":
		return &Aga{}, nil
	case "Twy":
		return &Twy{}, nil
	case "Tly":
		return &Tly{}, nil
	case "Apn":
		return &Apn{}, nil
	case "Aha":
		return &Aha{}, nil

	// Heliports
	case "Fto":
		return &Fto{}, nil
	case "Fdn":
		return &Fdn{}, nil
	case "Fdd":
		return &Fdd{}, nil
	case "Tla":
		return &Tla{}, nil
	case "Tls":
		return &Tls{}, nil
	// TODO
	case "Gsd":

	// Navigation
	case "Dpn":
		return &Dpn{}, nil
	case "Dme":
		return &Dme{}, nil
	case "Mkr":
		return &Mkr{}, nil
	case "Ndb":
		return &Ndb{}, nil
	case "Tcn":
		return &Tcn{}, nil
	case "Vor":
		return &Vor{}, nil

	// airspace
	case "Ase":
		return &Ase{}, nil
	case "Abd":
		return &Abd{}, nil
	// missing Avx
	case "Adg":
		return &Adg{}, nil
	case "Sae":
		return &Sae{}, nil

	// Geographical border
	case "Gbr":
		return &Gbr{}, nil
	// Geographical border vertex // not a standalone feature
	case "Gbv":

	// Obstacle Group
	case "Ogr":
		return &Ogr{}, nil

	// Obstacle
	case "Obs":
		return &Obs{}, nil

	// OFM Label Marker
	case "Lbm":
		return &Lbm{}, nil
	// OFM Plate Achive?
	case "Ppa":
		//TODO
		//return &Ppa{}, nil
	// OFM Platepackage
	case "Plp":
		//TODO
		//return &Plp{}, nil
	// OFM Procedure
	case "Prc":
		// TODO
		//return &Prc{}, nil
	// Misc structure
	case "Msc":
		return &Msc{}, nil
	default:
	}
	return nil, errors.New("unknown type " + name)
}

func UpdateReferences(fmap FeatureMap) {
	for _, f := range fmap {

		// set uid self-reference
		reflect.ValueOf(f.Uid()).Elem().FieldByName("IRef").Set(reflect.ValueOf(f))

		// update direct references
		s := reflect.ValueOf(f).Elem()
		for i := 0; i < s.NumField(); i++ {
			fld := s.Field(i)

			if fld.Kind() == reflect.Ptr {
				if fld.IsNil() {
					continue
				}
				if uid, ok := fld.Interface().(FeatureUid); ok {
					// replace with reference
					if ref, ok := fmap[uid.Hash()]; ok {
						fld.Set(reflect.ValueOf(ref.Uid()))
					} else {
						log.Printf("reference not found: %s => %s", f.Uid().String(), uid.String())
					}
				}
			}
		}

		// update Uid references
		s = reflect.ValueOf(f.Uid()).Elem()
		for i := 0; i < s.NumField(); i++ {
			fld := s.Field(i)

			if fld.Kind() == reflect.Ptr {
				if fld.IsNil() {
					continue
				}
				if uid, ok := fld.Interface().(FeatureUid); ok {
					// replace with reference
					if ref, ok := fmap[uid.Hash()]; ok {
						fld.Set(reflect.ValueOf(ref.Uid()))
					} else {
						log.Printf("reference not found: %s => %s", f.Uid().String(), uid.String())
					}
				}
			}
		}

		// loop through types
		switch v := f.(type) {
		case *Ahu:
			// backlink
			if a, ok := fmap[v.AhuUid.AhpUid.Hash()]; ok {
				ahp := a.(*Ahp)
				ahp.Ahu = append(ahp.Ahu, v)
			}
		case *Adg:
			// backlink
			if a, ok := fmap[v.AdgUid.AseUid.Hash()]; ok {
				ase := a.(*Ase)
				ase.Adg = v
			}
		}
	}
}
