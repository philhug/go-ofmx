package types

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

/*
  <Uni source="LF|AD|1.6|2018-01-04|1846">
    <UniUid region="LF" mid="d105b6b6-e020-2a73-5ec9-5eb3d5d86499">
      <txtName>STRASBOURG</txtName>
      <codeType>APP</codeType>
    </UniUid>
    <OrgUid region="LF" mid="971ba0a9-3714-12d5-d139-d26d5f1d6f25">
      <txtName>FRANCE</txtName>
    </OrgUid>
    <AhpUid region="LF" mid="561c1da4-3ba9-4389-08a3-2b83c9fc8d07">
      <codeId>LFST</codeId>
    </AhpUid>
    <codeClass>ICAO</codeClass>
    <txtRmk>APP split in two sectors east and west</txtRmk>
  </Uni>

  <Ser source="LF|AD|1.6|2018-01-04|1846">
    <SerUid mid="6a3ceb1b-5bbf-1fe4-8926-6fddd6bd1169">
      <UniUid region="LF" mid="d105b6b6-e020-2a73-5ec9-5eb3d5d86499">
        <txtName>STRASBOURG</txtName>
        <codeType>APP</codeType>
      </UniUid>
      <codeType>APP</codeType>
      <noSeq>1</noSeq>
    </SerUid>
    <Stt>
      <codeWorkHr>H24</codeWorkHr>
    </Stt>
    <txtRmk>aka STRASBOURG approche</txtRmk>
  </Ser>

*/

func TestOrgUidHash(t *testing.T) {
	exampleRegion := RegionalUid{Region: "LF"}
	exampleOrg := OrgUid{RegionalUid: exampleRegion, TxtName: "FRANCE"}
	assert.Equal(t, "OrgUid|LF|FRANCE", exampleOrg.String())
	assert.Equal(t, "9a01d0d0-b6e5-b0c3-a865-c3de9d3d98d4", exampleOrg.Hash())
}

func TestUniUidHash(t *testing.T) {
	exampleRegion := RegionalUid{Region: "LF"}
	exampleUni := UniUid{RegionalUid: exampleRegion, TxtName: "STRASBOURG", CodeType: "APP"}
	assert.Equal(t, "UniUid|LF|STRASBOURG|APP", exampleUni.String())
	assert.Equal(t, "5485e2c7-3cf3-1c25-d75d-feb92718eece", exampleUni.Hash())
}

func TestSerUidHash(t *testing.T) {
	exampleRegion := RegionalUid{Region: "LF"}
	exampleUni := UniUid{RegionalUid: exampleRegion, TxtName: "STRASBOURG", CodeType: "APP"}
	exampleSer := SerUid{UniUid: &exampleUni, CodeType: "APP", NoSeq: 1}

	assert.Equal(t, "SerUid|LF|STRASBOURG|APP|APP|1", exampleSer.String())
	assert.Equal(t, "0450ce19-1e8c-3cbf-e686-3dfde4c3b93a", exampleSer.Hash())
}

func TestFqyUidHash(t *testing.T) {
	exampleRegion := RegionalUid{Region: "LF"}
	exampleUni := UniUid{RegionalUid: exampleRegion, TxtName: "STRASBOURG", CodeType: "APP"}
	exampleSer := SerUid{UniUid: &exampleUni, CodeType: "APP", NoSeq: 1}
	exampleFqy := FqyUid{SerUid: &exampleSer, ValFreqTrans: "134.575"}

	assert.Equal(t, "FqyUid|LF|STRASBOURG|APP|APP|1|134.575", exampleFqy.String())
	assert.Equal(t, "269f4397-8992-17a9-eba0-0e75082c11dc", exampleFqy.Hash())
}
